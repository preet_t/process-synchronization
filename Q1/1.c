#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>

sem_t plock, rlock, organizer;
pthread_mutex_t referee, player, group, available;
int n, tot_players = 0, tot_referees = 0, c = 0, meet = 0;

void refereeMeetOrganizer (int ref)
{
	sem_wait(&organizer);
	printf("referee %d meets organizer\n", ref);
	sem_post(&organizer);
}

void playerMeetOrganizer (int play)
{
	sem_wait(&organizer);
	printf("player %d meets organizer\n", play);
	sem_post(&organizer);	
}

void refereeEnterCourt (int ref)
{
	int flg = 0;

	sem_wait(&rlock);
	
	pthread_mutex_lock(&group);
	++c;
	pthread_mutex_unlock(&group);
	
	while (!flg) {
		pthread_mutex_lock(&group);
		if (c == 3) flg = 1;
		pthread_mutex_unlock(&group);
	}

	sem_wait(&organizer);

	pthread_mutex_lock(&available);
	meet = 1;
	pthread_mutex_unlock(&available);
	
	printf("referee %d enters the court and adjusts the equipments\n", ref);
}

void playerEnterCourt (int play)
{
	int flg = 0;
	
	sem_wait(&plock);
	
	pthread_mutex_lock(&group);
	++c;
	pthread_mutex_unlock(&group);
	
	while (!flg) {
		pthread_mutex_lock(&group);
		if (c == 3) flg = 1;
		pthread_mutex_unlock(&group);
	}
	
	flg = 0;
	while(!flg) {
		pthread_mutex_lock(&available);
		if (meet == 1) flg = 1;
		pthread_mutex_unlock(&available);
	}
	
	printf("player %d enters the court and starts warm up\n", play);
	
	sleep(1);
	
	pthread_mutex_lock(&group);
	c--;
	pthread_mutex_unlock(&group);
}

void startGame (int ref)
{
	int flg = 0;

	sleep(0.5);
				
	pthread_mutex_lock(&group);
	c--;
	pthread_mutex_unlock(&group);
	
	flg = 0;
	while (!flg) {
		pthread_mutex_lock(&group);
		if (c == 0) flg = 1;
		pthread_mutex_unlock(&group);
	}
	
	printf("referee %d starts the game\n", ref);
	
	pthread_mutex_lock(&available);
	meet = 0;
	pthread_mutex_unlock(&available);
	
	sem_post(&organizer);
	sem_post(&rlock);
	sem_post(&plock);
	sem_post(&plock);
	
	printf("organizer is now free\n");
}

void* refereeEnterAcademy ()
{
	int ref;
	pthread_mutex_lock(&referee);
	ref = ++tot_referees;
	printf("referee %d arrived\n", tot_referees);
	pthread_mutex_unlock(&referee);
	refereeMeetOrganizer(ref);
	refereeEnterCourt(ref);
	startGame(ref);
}

void* playerEnterAcademy ()
{
	int play;
	pthread_mutex_lock(&player);
	play = ++tot_players;
	printf("player %d arrived\n", tot_players);
	pthread_mutex_unlock(&player);
	playerMeetOrganizer(play);
	playerEnterCourt(play);
}

int main ()
{
	printf("Gopichand Badminton Academy\n");
	int M = 32003;
	srand(time(0));
	scanf("%d", &n);
	pthread_t threads[1000];
	sem_init(&plock, 0, 2);
	sem_init(&rlock, 0, 1);
	sem_init(&organizer, 0, 1);
	for (int i=0, rem_players, rem_referees, x, p, type; i<3*n; ++i) {
		sleep(rand() % 3);
		rem_referees = n - tot_referees;
		rem_players = 2 * n - tot_players;
		p = M * ((float)rem_players / (rem_players + rem_referees));
		x = 1 + (rand() % M);
		if (x <= p)
			pthread_create(&threads[i], NULL, playerEnterAcademy, NULL);
		else
			pthread_create(&threads[i], NULL, refereeEnterAcademy, NULL);
		
	}
	for (int i=0; i<3*n; ++i)
		pthread_join(threads[i], NULL);
	sem_destroy(&plock);
	sem_destroy(&rlock);
	sem_destroy(&organizer);
	return 0;
}