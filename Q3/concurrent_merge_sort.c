#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/wait.h>
#include <sys/types.h>
#define MIN -1000000000
#define MAX 100000

void merge (int l, int r, int mid, long long int *a)
{
	int i = l, j = mid + 1, k = 0, aux[MAX+5];
	while (i <= mid && j <= r)
		if (a[i] <= a[j]) aux[k++] = a[i++];
		else aux[k++] = a[j++];
	while (i <= mid)
		aux[k++] = a[i++];
	while (j <= r)
		aux[k++] = a[j++];
	for (i=l; i<=r; i++)
		a[i] = aux[i-l];
}

void selection_sort (int l, int r, long long int *a)
{
	for (int i=0, ind, temp; i<r-l+1; ++i)
	{
		long long int mx = MIN;
		for (int j=l; j<=r-i; ++j)
			if (a[j] > mx)
			{
				mx = a[j];
				ind = j;
			}
		temp = a[r-i];
		a[r-i] = a[ind];
		a[ind] = temp;
	}
}

void split (int l, int r, long long int *a)
{
	if (r - l + 1 <= 5)
		selection_sort(l, r, a);
	else
	{
		int mid = l + (r - l) / 2, status;
		pid_t pid1, pid2;
		pid1 = fork();
		if (pid1 == -1)
		{
			perror("fork");
			exit(-1);
		}
		else if (pid1 == 0)
		{
			split(l, mid, a);
			exit(0);
		}
		else
		{
			pid2 = fork();
			if (pid2 == -1)
			{
				perror("fork");
				exit(-1);
			}
			else if (pid2 == 0)
			{
				split(1 + mid, r, a);
				exit(0);
			}
			waitpid(pid1, &status, WUNTRACED);
			waitpid(pid2, &status, WUNTRACED);
		}
		merge(l, r, mid, a);
	}
}

int main ()
{
	long long int n, *a;
	
	scanf("%lld", &n);
	
	int mem_id;
	if ((mem_id = shmget(IPC_PRIVATE, n * sizeof(long long int), IPC_CREAT | 0777)) == -1)
	{
		perror("shmget");
		exit(EXIT_FAILURE);
	}
	
	if ((a = shmat(mem_id, NULL, SHM_EXEC)) == (void*)(-1))
	{
		perror("shmat");
		exit(EXIT_FAILURE);
	}

	for (int i=0; i<n; ++i)
		scanf("%lld", &a[i]);
	
	split(0, n-1, a);
	
	for (int i=0; i<n; ++i)
		printf("%lld ", a[i]);
	printf("\n");

	if (shmdt(a) != 0)
	{
		perror("shmdt");
		exit(EXIT_FAILURE);
	}

	if (shmctl(mem_id, IPC_RMID, NULL) == -1)
	{
		perror("shmctl");
		exit(EXIT_FAILURE);
	}
	return 0;
}