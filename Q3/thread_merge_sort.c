#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <pthread.h>
#define MIN -1000000000
#define MAX 100000

typedef struct split_arguments {
	int l, r;
	long long int *a;
}sp_args;

void merge (int l, int r, int mid, long long int *a)
{
	int i = l, j = mid + 1, k = 0, aux[MAX+5];
	while (i <= mid && j <= r)
		if (a[i] <= a[j]) aux[k++] = a[i++];
		else aux[k++] = a[j++];
	while (i <= mid)
		aux[k++] = a[i++];
	while (j <= r)
		aux[k++] = a[j++];
	for (i=l; i<=r; i++)
		a[i] = aux[i-l];
}

void selection_sort (int l, int r, long long int *a)
{
	for (int i=0, ind, temp; i<r-l+1; ++i)
	{
		long long int mx = MIN;
		for (int j=l; j<=r-i; ++j)
			if (a[j] > mx)
			{
				mx = a[j];
				ind = j;
			}
		temp = a[r-i];
		a[r-i] = a[ind];
		a[ind] = temp;
	}
}

void *split (void *args)
{
	sp_args *arg = (sp_args*)args;

	long long int *a = arg->a;
	int l = arg->l, r = arg->r;
	if (r - l + 1 <= 5)
		selection_sort(l, r, a);
	else
	{
		int mid = l + (r - l) / 2;
		pthread_t thread1, thread2;
		sp_args arg1, arg2;
		arg1.l = l;
		arg1.r = mid;
		arg2.l = mid + 1;
		arg2.r = r;
		arg1.a = arg2.a = a;
		if (pthread_create(&thread1, NULL, split, (void*)(&arg1)))
		{
			perror("pthread thread1");
			exit(EXIT_FAILURE);
		}
		if (pthread_create(&thread2, NULL, split, (void*)(&arg2)))
		{
			perror("pthread thread2");
			exit(EXIT_FAILURE);
		}
		pthread_join(thread1, NULL);
		pthread_join(thread2, NULL);
		merge(l, r, mid, a);
	}
}

int main ()
{
	long long int n, *a;
	
	scanf("%lld", &n);
	
	int mem_id;
	if ((mem_id = shmget(IPC_PRIVATE, n * sizeof(long long int), IPC_CREAT | 0777)) == -1)
	{
		perror("shmget");
		exit(EXIT_FAILURE);
	}
	
	if ((a = shmat(mem_id, NULL, SHM_EXEC)) == (void*)(-1))
	{
		perror("shmat");
		exit(EXIT_FAILURE);
	}

	for (int i=0; i<n; ++i)
		scanf("%lld", &a[i]);
	
	sp_args arg;
	arg.l = 0;
	arg.r = n - 1;
	arg.a = a;
	split(&arg);
	
	for (int i=0; i<n; ++i)
		printf("%lld ", a[i]);
	printf("\n");

	if (shmdt(a) != 0)
	{
		perror("shmdt");
		exit(EXIT_FAILURE);
	}

	if (shmctl(mem_id, IPC_RMID, NULL) == -1)
	{
		perror("shmctl");
		exit(EXIT_FAILURE);
	}
	return 0;
}