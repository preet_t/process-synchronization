#!/bin/bash

gcc merge_sort.c
echo "--------------------------"
echo "SIMPLE MERGESORT"
echo "--------------------------"
time ./a.out < test.txt > normal_out.txt

gcc concurrent_merge_sort.c
echo "--------------------------"
echo "CONCURRENT MERGESORT"
echo "--------------------------"
time ./a.out < test.txt > concurrent_out.txt

gcc thread_merge_sort.c -pthread -lrt
echo "--------------------------"
echo "THREAD MERGESORT"
echo "--------------------------"
time ./a.out < test.txt > thread_out.txt

rm a.out