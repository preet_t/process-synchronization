#include <stdio.h>
#define MIN -1000000000
#define SZ_MAX 100000

void merge (int l, int r, int mid, long long int *a)
{
	int i = l, j = mid + 1, k = 0, aux[SZ_MAX];
	while (i <= mid && j <= r)
		if (a[i] <= a[j]) aux[k++] = a[i++];
		else aux[k++] = a[j++];
	while (i <= mid)
		aux[k++] = a[i++];
	while (j <= r)
		aux[k++] = a[j++];
	for (i=l; i<=r; i++)
		a[i] = aux[i-l];
}

void selection_sort (int l, int r, long long int *a)
{
	int ind;
	for (int i=0, temp; i<r-l+1; ++i)
	{
		long long int mx = MIN;
		for (int j=l; j<=r-i; ++j)
			if (a[j] > mx)
			{
				mx = a[j];
				ind = j;
			}
		temp = a[r-i];
		a[r-i] = a[ind];
		a[ind] = temp;
	}
}

void split (int l, int r, long long int *a)
{
	if (r - l + 1 > 5)
	{
		int mid = l + (r - l) / 2;
		split(l, mid, a);
		split(1 + mid, r, a);
		merge(l, r, mid, a);
	}
	else selection_sort(l, r, a);
}

int main ()
{
	long long int n, a[100000];
	
	scanf("%lld", &n);
	for (int i=0; i<n; ++i)
		scanf("%lld", &a[i]);
	
	split(0, n-1, a);
	
	for (int i=0; i<n; ++i)
		printf("%lld ", a[i]);
	printf("\n");
}